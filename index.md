---
title: "Accueil"
order: 0
in_menu: true
---
# L'oignon fait la force

Sacralisation de la lutte contre l'A69, les jardins maraîchers de l'oignon fait la force existent depuis la manifestation d'avril 2023 sur des lieux emblématiques de la lutte tel que la réserve naturelle du Dicosa (dans le Tarn proche de Castres).

Ces jardins maraîchers publics et pédagogiques sont des supports pour parler avec espoir d’une agriculture protectrice du vivant !

[Jusqu'au 15 janvier 2024 : Votez pour nous au budget participatif régional en cliquant ici !](https://jeparticipe.laregioncitoyenne.fr/project/budget-participatif-2023-1/collect/proposez-vos-projets/proposals/loignon-fait-la-force-64dcf3c5373c7)


## Nos activités

Ce que nous souhaitons faire

### Accueil du jeune public

Guidés par des bénévoles, des jeunes seront accueillis pour découvrir cette agriculture protectrice du vivant. A l’occasion d'animations, la venue d’intervenants spécifiques (artistes, photographes, botanistes, agriculteur.rice.s engagés pour le vivant), permettront d’apporter aux jeunes un nouveau regard, plus léger et porteur d’espoir, sur l’agriculture de demain.

### Entretien d'un jardin potager

Entretenu par des bénévoles engagés tout au long de l’année, le jardin sera un support de production et de communication intéressant pour promouvoir une agriculture protectrice du vivant.

### Evénements et discussions

Au cours de l'année, différents événements (ciné-débats, groupe d'échanges, soirées conférence/concerts, etc.) permettront d'accueillir un plus large public afin de discuter et dessiner ensemble le monde que nous voulons demain.

## Impliquez-vous

Vous voulez nous aider à réaliser notre projet ?

Contactez nous pour devenir bénévole et nous rejoindre mettre les mains dans la terre.

Vous pouvez aussi voter pour nous au budget participatif de la Région (jusqu'au 15 janvier 2024 !!)

Notre projet s'inscrit dans la lutte contre l'A69, alors s'impliquer dans la lutte c'est aussi nous aider ! 